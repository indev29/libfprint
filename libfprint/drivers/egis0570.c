#define FP_COMPONENT "egis0570"

#include "drivers_api.h"
#include "egis0570.h"

typedef struct
{
  size_t total;
  unsigned char (*packets)[EGIS0570_PKT_SIZE];
} PacketSet;

typedef enum {
  EXCHANGE_INITIAL,
  EXCHANGE_REPEAT
} ExchangeTypes;

static const PacketSet egis0570_init_pkts   = { EGIS0570_INIT_TOTAL, _egis0570_init_pkts };
static const PacketSet egis0570_repeat_pkts = { EGIS0570_REPEAT_TOTAL, _egis0570_repeat_pkts };

struct _FpiDeviceEgis0570
{
  FpImageDevice       parent;

  ExchangeTypes       exchange_type;
  FpiImageDeviceState capture_state;

  gboolean            activating;
  gboolean            deactivating;
};
G_DECLARE_FINAL_TYPE (FpiDeviceEgis0570, fpi_device_egis0570, FPI, DEVICE_EGIS0570, FpImageDevice);
G_DEFINE_TYPE (FpiDeviceEgis0570, fpi_device_egis0570, FP_TYPE_IMAGE_DEVICE);

static size_t
count_buffer_average (guchar * buffer, size_t size)
{
  size_t sum = 0;

  for (size_t i = 0; i < size; ++i)
    sum += buffer[i];
  return sum / size;
}

static gboolean
finger_is_present (FpImage * img)
{
  guint8 avg = count_buffer_average (img->data, img->width * img->height);

  fp_dbg ("avg %u", avg);
  return avg > EGIS0570_FINGER_PRESENCE_THRESHOLD;
}

static const PacketSet *
decide_packet_set (ExchangeTypes exchange_type)
{
  return (exchange_type == EXCHANGE_INITIAL) ? &egis0570_init_pkts :
         &egis0570_repeat_pkts;
}

static gboolean
is_image_expected (ExchangeTypes exchange_type, int progress)
{
  return progress == decide_packet_set (exchange_type)->total - 1;
}


static void async_transfer_in (FpiSsm   * ssm,
                               FpDevice * dev,
                               size_t     size);

static void
async_transfer_cb (FpiUsbTransfer * transfer, FpDevice * dev, gpointer user_data, GError * error)
{
  FpiSsm * ssm = transfer->ssm;

  if (error)
    {
      fpi_ssm_mark_failed (ssm, error);
      return;
    }

  FpiDeviceEgis0570 * self = FPI_DEVICE_EGIS0570 (dev);

  if (self->deactivating)
    {
      fpi_ssm_mark_completed (ssm);
      return;
    }

  int progress = fpi_ssm_get_cur_state (ssm);
  gboolean image_expected = is_image_expected (self->exchange_type, progress);

  switch (transfer->endpoint)
    {
    case EGIS0570_EPOUT:
      {
        if (image_expected)
          async_transfer_in (ssm, dev, EGIS0570_IMGPKT_SIZE);
        else
          async_transfer_in (ssm, dev, EGIS0570_PKT_SIZE);
        break;
      }

    case EGIS0570_EPIN:
      {
        if (image_expected && !self->activating && self->exchange_type != EXCHANGE_INITIAL)
          {
            // TODO: cleanup

            // Note that device returns five images at a time
            // We use only the first one from the pack. Do we need more?
            FpImage * img = fp_image_new (EGIS0570_IMGWIDTH, EGIS0570_IMGHEIGHT);
            /*FpImage * img = fp_image_new(EGIS0570_IMGWIDTH, EGIS0570_IMGHEIGHT*EGIS0570_IMGNUM);*/
            /*img->flags = FPI_IMAGE_COLORS_INVERTED;*/

            guchar * best_frame = NULL;
            guint8 best_frame_avg = 0;
            for (int i = 0; i < EGIS0570_IMGNUM; ++i)
              {
                guchar * frame = transfer->buffer + EGIS0570_IMGSIZE * i;
                guint8 avg = count_buffer_average (frame, EGIS0570_IMGSIZE);
                if (avg > best_frame_avg)
                  {
                    best_frame = frame;
                    best_frame_avg = avg;
                  }
              }
            memcpy (img->data, best_frame, EGIS0570_IMGSIZE);
            /*memcpy(img->data, transfer->buffer, EGIS0570_IMGSIZE*EGIS0570_IMGNUM);*/

            gboolean finger_present = finger_is_present (img);
            if (!finger_present || self->capture_state != FPI_IMAGE_DEVICE_STATE_AWAIT_FINGER_OFF)
              {
                FpImageDevice * imgdev = FP_IMAGE_DEVICE (dev);
                fpi_image_device_report_finger_status (imgdev, finger_present);
                if (finger_present)
                  fpi_image_device_image_captured (imgdev, img);
              }
          }
        fpi_ssm_next_state (ssm);
        break;
      }
    }
}

static void
async_transfer_in (FpiSsm * ssm, FpDevice * dev, size_t size)
{
  FpiUsbTransfer * transfer = fpi_usb_transfer_new (dev);

  transfer->ssm = ssm;
  transfer->short_is_error = TRUE;

  fpi_usb_transfer_fill_bulk (transfer, EGIS0570_EPIN, size);
  fpi_usb_transfer_submit (transfer, EGIS0570_BULK_TIMEOUT, NULL, async_transfer_cb, NULL);
}

static void
async_transfer_out (FpiSsm * ssm, FpDevice * dev, guint8 * buffer, size_t size)
{
  FpiUsbTransfer * transfer = fpi_usb_transfer_new (dev);

  transfer->ssm = ssm;
  transfer->short_is_error = TRUE;

  fpi_usb_transfer_fill_bulk_full (transfer, EGIS0570_EPOUT, buffer, size, NULL);

  fpi_usb_transfer_submit (transfer, EGIS0570_BULK_TIMEOUT, NULL, async_transfer_cb, NULL);
}

static void
async_exchange_loop (FpiSsm * ssm, FpDevice * dev)
{
  FpiDeviceEgis0570 * self = FPI_DEVICE_EGIS0570 (dev);

  if (self->deactivating)
    {
      fpi_ssm_mark_completed (ssm);
      return;
    }

  FpiUsbTransfer * transfer = fpi_usb_transfer_new (dev);

  transfer->ssm = ssm;
  transfer->short_is_error = TRUE;

  const PacketSet * packet_set = decide_packet_set (self->exchange_type);
  int progress = fpi_ssm_get_cur_state (ssm);

  async_transfer_out (ssm, FP_DEVICE (dev), (guint8 *) packet_set->packets[progress], EGIS0570_PKT_SIZE);
}

static void
async_exchange (FpImageDevice * dev, ExchangeTypes exchange_type, FpiSsmCompletedCallback callback)
{
  FpiDeviceEgis0570 * self = FPI_DEVICE_EGIS0570 (dev);

  self->exchange_type = exchange_type;

  if (self->deactivating)
    return;

  size_t total = decide_packet_set (exchange_type)->total;

  FpiSsm * ssm = fpi_ssm_new (FP_DEVICE (dev), async_exchange_loop, total);

  fpi_ssm_start (ssm, callback);
}

static void dev_capture (FpImageDevice *dev);

static void
dev_capture_complete (FpiSsm *ssm, FpDevice *dev, GError *error)
{
  FpImageDevice * fpi_dev = FP_IMAGE_DEVICE (dev);

  FpiDeviceEgis0570 * self = FPI_DEVICE_EGIS0570 (dev);

  if (self->deactivating)
    {
      self->activating = FALSE;
      self->deactivating = FALSE;
      fpi_image_device_deactivate_complete (fpi_dev, NULL);
      return;
    }

  if (self->activating)
    {
      self->activating = FALSE;
      fpi_image_device_activate_complete (fpi_dev, NULL);
    }

  // Repeating until deactivated
  dev_capture (fpi_dev);
}

static void
dev_capture (FpImageDevice *dev)
{
  async_exchange (dev, EXCHANGE_REPEAT, dev_capture_complete);
}

static void
dev_activate (FpImageDevice *dev)
{
  FpiDeviceEgis0570 * self = FPI_DEVICE_EGIS0570 (dev);

  self->activating = TRUE;

  dev_capture (dev);
}

static void
dev_deactivate (FpImageDevice *dev)
{
  FpiDeviceEgis0570 * self = FPI_DEVICE_EGIS0570 (dev);

  self->deactivating = TRUE;
}

static void
dev_open_complete (FpiSsm *ssm, FpDevice *dev, GError *error)
{
  fpi_device_open_complete (dev, error);
}

static void
dev_open (FpImageDevice *dev)
{
  GError * error = NULL;

  do   //once
    {
      GUsbDevice * usbdev = fpi_device_get_usb_device (FP_DEVICE (dev));

      if (!g_usb_device_claim_interface (usbdev, EGIS0570_DEVICE_INTERFACE, 0, &error))
        break;

      if (!g_usb_device_set_configuration (usbdev, EGIS0570_DEVICE_CONFIGURATION, &error))
        break;

      /*
       * WARNING: Device resetting is very important
       * I recommend NOT to touch this line
       */
      if (!g_usb_device_reset (usbdev, &error))
        break;
    }
  while(FALSE);

  if (error)
    {
      fpi_image_device_open_complete (dev, error);
      return;
    }

  async_exchange (dev, EXCHANGE_INITIAL, dev_open_complete);
}

static void
dev_close (FpImageDevice *dev)
{
  GError * error = NULL;

  GUsbDevice * usbdev = fpi_device_get_usb_device (FP_DEVICE (dev));

  g_usb_device_release_interface (usbdev, EGIS0570_DEVICE_INTERFACE, 0, &error);

  fpi_image_device_close_complete (dev, error);
}

static void
dev_change_state (FpImageDevice * dev, FpiImageDeviceState state)
{
  FpiDeviceEgis0570 * self = FPI_DEVICE_EGIS0570 (dev);

  self->capture_state = state;
}

static const FpIdEntry id_table[] = {
  /* EgisTec (aka LighTuning) 0570 */
  { .vid = EGIS0570_VENDOR_ID,  .pid = EGIS0570_PRODUCT_ID, },
  { .vid = 0,  .pid = 0,  .driver_data = 0 },
};

static void
fpi_device_egis0570_init (FpiDeviceEgis0570 *self)
{
}
static void
fpi_device_egis0570_class_init (FpiDeviceEgis0570Class * class)
{
  FpDeviceClass *dev_class = FP_DEVICE_CLASS (class);

  dev_class->id = FP_COMPONENT;
  dev_class->full_name = "EgisTec Touch Fingerprint Sensor 0570";
  dev_class->type = FP_DEVICE_TYPE_USB;
  dev_class->id_table = id_table;
  dev_class->scan_type = FP_SCAN_TYPE_PRESS;

  FpImageDeviceClass * img_class = FP_IMAGE_DEVICE_CLASS (class);

  img_class->img_open = dev_open;
  img_class->img_close = dev_close;
  img_class->activate = dev_activate;
  img_class->deactivate = dev_deactivate;
  img_class->change_state = dev_change_state;

  img_class->img_width = EGIS0570_IMGWIDTH;
  img_class->img_height = EGIS0570_IMGHEIGHT;
  img_class->bz3_threshold = EGIS0570_BZ3_THRESHOLD;
}
